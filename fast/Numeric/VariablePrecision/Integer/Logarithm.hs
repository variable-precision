{-# LANGUAGE MagicHash #-}
module Numeric.VariablePrecision.Integer.Logarithm where

import GHC.Exts (Int(I#))
import GHC.Integer.Logarithms (integerLog2#)

integerLog2 :: Integer -> Int
integerLog2 n = I# (integerLog2# n)
