{-# LANGUAGE BangPatterns #-}
module Numeric.VariablePrecision.Integer.Logarithm where

import Data.Bits (shiftL)

integerLog2 :: Integer -> Int
integerLog2 n
  | n > 0 = go (-1) 1
  | otherwise = error $ "integerLog2: non-positive argument: " ++ show n
  where
    go !l !b
      | n < b = l
      | otherwise = go (l + 1) (b `shiftL` 1)
